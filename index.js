require("dotenv").config();
const express = require("express");
const multer = require("multer");
const uuid = require("uuid").v4
const { message } = require("statuses");
const { s3uploadv2 } = require("./s3service");
const app = express();
require ("./s3service");

// const upload = multer({dest: "uploads/"});
// app.post("/upload",upload.single("file"),(req,res) =>{
//     res.send("singlw image is uploaded")
// });

// const storage = multer.diskStorage({
//     destination:(req, file,cb) => {
//         cb(null, "uploads");
//     },
//     filename: (req, file,cb) => {
//         const { originalname } =file;
//         cb(null, `${uuid()}-${originalname}`);
//     }
// })

const storage = multer.memoryStorage()

const fileFilter = (req, file, cb) => {
    if (file.mimetype.split("/")[0] === "image") {
        cb(null, true);
    } else {
        cb(new multer.MulterError("LIMIT_UNEXPECTED_FILE"), false);
    }
};

const upload = multer({
    storage,
    fileFilter,
    limits: { fieldSize: 100000000, files: 5 },
});
app.post("/upload", upload.array("file"), async (req, res) => {
    // const file = req.files[0];
    try{
        const result = await s3uploadv2(req.files);
        console.log(result);
        return res.send({status:"success", result})
    }catch (err) {
        console.log(err)
    }   
});

app.use((error, req, res, next) => {
    if (error instanceof multer.MulterError) {
        if (error.code === "LIMIT_FILE_SIZE") {
            return res.status(400).json({
                message: "file is to large",
            })
        }

        if (error.code === "LIMIT_FILE_COUNT") {
            return res.status(400).json({
                message: "file limit reached",
            })
        }

        if (error.code === "LIMIT_UNEXPECTED_FILE") {
            return res.status(400).json({
                message: "file must be an image",
            })
        }
    }
});
app.listen(4000, () => console.log("listening on port 4000"));