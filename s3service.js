const { S3 } = require("aws-sdk");
const uuid = require("uuid").v4
var Promise = require('promise');

exports.s3uploadv2 = async (files) => {
    const s3 = new S3();
    //single file uploaded
//     const param = {
//         Bucket: process.env.AWS_BUCKET_NAME,
//         Key: `uploads/${uuid()}-${file.originalname}`,
//         Body: file.buffer
//     };
    // return await s3.upload(param).promise();
    
    //multiple file uploaded
    const params = files.map(file => {
        return{
        Bucket: process.env.AWS_BUCKET_NAME,
        Key: `uploads/${uuid()}-${file.originalname}`,
        Body: file.buffer,
        };
    });
    // let promise;
    const result = await Promise.all(
        params.map((param)=> s3.upload(param).promise())
    ); 
    return result;
};
